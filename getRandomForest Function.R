# Function to build random forest model on training set and test on test set
# Input -> dataframe
# Output -> list of confusion matrix and variable importance 
# Created Jan. 25th 
# Updated Feb. 1st: updated numtrees (600) and mtry (33)
#                 : Updated confusion matrix to order Low -> High
#         Feb. 5th: Took out head and tail from return. Function not returns var.importance list

# Load packages
require(readr) # read in data
require(ranger) # fast implementation of RF 
require(rpart) # used for dataset separation (train, validation and test)
require(caret) # can be used for cross validation and metrics
require(Metrics) # used for analysis 

# Create function to subset dataset, build model on training set and output confusion matrix and variable importance
getRandomForest <- function(data, seed, sample.size){
  
  # Create training and testing set splits
  set.seed(seed)
  smp_size <- floor(sample.size * nrow(data))
  train_index <- sample(seq_len(nrow(data)), size = smp_size)
  
  train <- data[train_index, ]
  test <- data[-train_index, ]
  
  # Create random forest model
  model <- ranger(DAY0 ~., 
                  data = train, 
                  num.trees = 600,
                  mtry = 33, 
                  importance = "impurity", 
                  min.node.size = 1,
                  splitrule = "gini",
                  verbose = TRUE)
  # predict danger rating from test set
  drPred <- predict(model, data = test)
  
  # create objects for predicted and actual data
  data.predicted <- factor(drPred$predictions, levels = c("Low", "Moderate", "Considerable", "High", "Extreme"))
  data.reference <- factor(test$DAY0, levels = c("Low", "Moderate", "Considerable", "High", "Extreme"))
  
  conf_matrix <- confusionMatrix(data = data.predicted,
                                 reference = data.reference)
  return(list(conf_matrix, sort(model$variable.importance, decreasing = TRUE)))
}
